myip() {
  local ipv4 ipv6 ptr4 ptr6
  ipv4=$(curl --silent -4 https://icanhazip.com/)
  ptr4=$(curl --silent -4 https://icanhazptr.com/)
  ipv6=$(curl --silent -6 https://icanhazip.com/)
  ptr6=$(curl --silent -6 https://icanhazptr.com/)
  [[ -z $ipv4 && -z $ptr4 ]] && echo "No IPv4 internet connectivity" || echo "IPv4: $ipv4 ($ptr4)"
  [[ -z $ipv6 && -z $ptr6 ]] && echo "No IPv6 internet connectivity" || echo "IPv6: $ipv6 ($ptr6)"
}

function parse_git_branch() {
  BRANCH=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
  if [ ! "${BRANCH}" == "" ]
  then
    STAT=$(parse_git_dirty)
    echo "[${BRANCH}${STAT}]"
  else
    echo ""
  fi
}

# get current status of git repo
function parse_git_dirty {
  status=$(git status 2>&1 | tee)
  dirty=$(echo -n "${status}" 2> /dev/null | grep "modified:" &> /dev/null; echo "$?")
  untracked=$(echo -n "${status}" 2> /dev/null | grep "Untracked files" &> /dev/null; echo "$?")
  ahead=$(echo -n "${status}" 2> /dev/null | grep "Your branch is ahead of" &> /dev/null; echo "$?")
  newfile=$(echo -n "${status}" 2> /dev/null | grep "new file:" &> /dev/null; echo "$?")
  renamed=$(echo -n "${status}" 2> /dev/null | grep "renamed:" &> /dev/null; echo "$?")
  deleted=$(echo -n "${status}" 2> /dev/null | grep "deleted:" &> /dev/null; echo "$?")
  bits=''
  if [ "${renamed}" == "0" ]; then
    bits=">${bits}"
  fi
  if [ "${ahead}" == "0" ]; then
    bits="*${bits}"
  fi
  if [ "${newfile}" == "0" ]; then
    bits="+${bits}"
  fi
  if [ "${untracked}" == "0" ]; then
    bits="?${bits}"
  fi
  if [ "${deleted}" == "0" ]; then
    bits="x${bits}"
  fi
  if [ "${dirty}" == "0" ]; then
    bits="!${bits}"
  fi
  if [ ! "${bits}" == "" ]; then
    echo " ${bits}"
  else
    echo ""
  fi
}

export PS1="\[\e[35m\]\t\[\e[m\] \[\e[36m\]\u\[\e[m\]\[\e[36m\]@\[\e[m\]\[\e[36m\]\h\[\e[m\]:\[\e[36m\]\w\[\e[m\]\[\e[32m\]\`parse_git_branch\`\[\e[m\]> "
export CLICOLOR=1

alias ls='exa'
alias ll='exa -alBhg'
alias lt='ll -T'
alias vi='vim'
alias svi='sudo vim'
alias chkspeed='wget http://speedtest.tele2.net/1GB.zip -O /dev/null'
alias timestamp='date +%Y-%m-%dT%H-%M-%S'
alias getmyip=myip
alias cd..='cd ..' # common typing error
alias ..='cd ..' # i'm sometimes lazy
alias ...='cd ../../../' # really lazy
alias ....='cd ../../../../' # really really lazy
alias .....='cd ../../../../../' # not kidding
alias .4='cd ../../../../' # if you can script it
alias .5='cd ../../../../..' # just script it
