if [ -f ~/.bashrc ]; then
   source ~/.bashrc
fi

export PATH="/usr/local/bin"
export PATH="/usr/bin:$PATH"
export PATH="/bin:$PATH"
export PATH="/usr/sbin:$PATH"
export PATH="/sbin:$PATH"
export PATH="~/bin:$PATH"
export LDFLAGS="-L/usr/local/opt/openssl/lib"
export CPPFLAGS="-I/usr/local/opt/openssl/include"
export PKG_CONFIG_PATH="/usr/local/opt/openssl/lib/pkgconfig"

#count=0
#picture_array=()
#for filename in ~/Pictures/ilya-kuvshinov/*
#do
#  let "count=$count+1"
#  picture_array[count]=$filename
#done
#neofetch --iterm2 --source ${picture_array[$RANDOM % ${#picture_array[@]} ]}

neofetch
